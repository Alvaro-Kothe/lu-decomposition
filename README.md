# LU Decomposition

My implementation of LU decomposition in python.

## LU

A square matrix can be decomposed into a product of a lower triangular matrix and
a upper triangular matrix.

In this package the permutation is

$$PA = LU,$$
where $P$ is a pivot for row permutation, $L$ the lower matrix and $U$ the upper matrix.

## Installation

To install the package either copy the file
[lu_decomposition.py](https://gitlab.com/Alvaro-Kothe/lu-decomposition/-/raw/master/lu_decomposition.py)
into your project or install with a python package manager.
For example with pip:

```console
$ pip install git+https://gitlab.com/Alvaro-Kothe/lu-decomposition
```

## Quickstart

This package provides a class `LU` that encapsulates the LU decomposition.
Basic usage consists of

```python
import lu_decomposition

matrix = [
    [1, 3, 5],
    [2, 4, 7],
    [1, 1, 0]
]

lu = lu_decomposition.LU().fit(matrix)
print(lu.lower)  # L component
print(lu.upper)  # U component
print(lu.pivot)  # Pivots
print(lu.solve([1, 2, 3]))  # Solves the system Ax = b
print(lu.solve())  # Computes the inverse of A.
print(lu.det())  # Computes the determinant of A.
```
