import pytest

import lu_decomposition


@pytest.mark.parametrize(
    ("matrix", "expected_L", "expected_U", "expected_P"),
    (
        (
            [[1, 3, 5], [2, 4, 7], [1, 1, 0]],
            [[1.0, 0.0, 0.0], [0.5, 1.0, 0.0], [0.5, -1.0, 1.0]],
            [[2.0, 4.0, 7.0], [0.0, 1.0, 1.5], [0.0, 0.0, -2.0]],
            [[0, 1, 0], [1, 0, 0], [0, 0, 1]],
        ),
        (
            [[11, 9, 24, 2], [1, 5, 2, 6], [3, 17, 18, 1], [2, 5, 7, 1]],
            [
                [1.0, 0.0, 0.0, 0.0],
                [0.27273, 1.0, 0.0, 0.0],
                [0.09091, 0.28750, 1.0, 0.0],
                [0.18182, 0.23125, 0.00360, 1.00000],
            ],
            [
                [11.0, 9.0, 24.0, 2.0],
                [0.0, 14.54545, 11.45455, 0.45455],
                [0.00000, 0.00000, -3.47500, 5.68750],
                [0.00000, 0.00000, 0.00000, 0.51079],
            ],
            [[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]],
        ),
    ),
)
def test_lu_factorization(
    matrix: list[list[float]],
    expected_L: list[list[float]],
    expected_U: list[list[float]],
    expected_P: list[list[float]],
) -> None:
    lu_ = lu_decomposition.LU().fit(matrix)
    assert lu_.lower is not None
    assert lu_.upper is not None
    assert lu_.pivot is not None

    for i in range(len(matrix)):
        assert lu_.lower[i] == pytest.approx(expected_L[i], abs=1e-5)
        assert lu_.upper[i] == pytest.approx(expected_U[i], abs=1e-5)

    assert lu_.pivot == expected_P


def test_singular() -> None:
    matrix = [[1.0, 2.0], [2.0, 4.0]]
    with pytest.raises(RuntimeError, match="^Matrix is singular$"):
        lu_decomposition.LU().fit(matrix)


@pytest.mark.parametrize(
    ("matrix", "expected"),
    (
        ([[1, 0], [0, 1]], [[1, 0], [0, 1]]),
        ([[2, 1], [4, 4]], [[1.0, -0.25], [-1.0, 0.5]]),
    ),
)
def test_inverse(matrix: list[list[float]], expected: list[list[float]]) -> None:
    lu_ = lu_decomposition.LU().fit(matrix)
    assert lu_.solve() == expected


@pytest.mark.parametrize(
    ("a", "b", "expected"),
    (
        ([[1, 2], [3, 5]], [1, 2], [-1, 1]),
        ([[2, 1, -1], [-3, -1, 2], [-2, 1, 2]], [8, -11, -3], [2, 3, -1]),
    ),
)
def test_solve_equation_system(
    a: list[list[float]], b: list[float], expected: list[float]
) -> None:
    lu_ = lu_decomposition.LU().fit(a)
    assert lu_.solve(b) == pytest.approx(expected)


@pytest.mark.parametrize(
    ("matrix", "expected"),
    (
        ([[1, 2], [3, 4]], -2.0),
        ([[55, 25, 15], [30, 44, 2], [11, 45, 77]], 137180),
    ),
)
def test_determinant(matrix: list[list[float]], expected: float) -> None:
    lu_ = lu_decomposition.LU().fit(matrix)
    assert lu_.det() == expected


def test_readme_example() -> None:
    matrix = [[1, 3, 5], [2, 4, 7], [1, 1, 0]]

    lu = lu_decomposition.LU().fit(matrix)
    print(lu.lower)  # L component
    print(lu.upper)  # U component
    print(lu.pivot)  # Pivots
    print(lu.solve([1, 2, 3]))  # Solves the system Ax = b
    print(lu.solve())  # Computes the inverse of A.
    print(lu.det())  # Computes the determinant of A.


@pytest.mark.parametrize(
    "matrix",
    ([[1, 2, 3], [2, 4, 6]], [[1, 2], [3, 4], [5, 6]], [[1, 2], [3]]),
)
def test_non_square_matrix(matrix: list[list[float]]) -> None:
    with pytest.raises(ValueError, match="^Matrix is not square$"):
        lu_decomposition.LU().fit(matrix)
