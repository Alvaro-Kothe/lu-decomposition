from __future__ import annotations

import sys
from collections.abc import Sequence

if sys.version_info < (3, 11):
    from typing import TypeVar

    Self = TypeVar("Self", bound="LU")
else:
    from typing import Self


Vector = Sequence[float]
Matrix = Sequence[Vector]


def _is_square_matrix(matrix: Matrix) -> bool:
    """Verify if matrix is square

    Check if the number of columns is equal to the number of rows.

    Args:
        matrix: Matrix.

    Returns:
        true if matrix is square, false otherwise.
    """
    nrow = len(matrix)
    return all(len(row) == nrow for row in matrix)


def _pivotize(matrix: Matrix) -> Matrix:
    size = len(matrix)
    eye = [[0.0] * size for _ in range(size)]
    for i in range(size):
        eye[i][i] = 1.0

    for i in range(size):
        pivot = max(range(i, size), key=lambda j: abs(matrix[j][i]))
        eye[i], eye[pivot] = eye[pivot], eye[i]
    return eye


def _matmul(mata: Matrix, matb: Matrix) -> Matrix:
    rows_a = len(mata)
    cols_b = len(matb[0])
    result = [[0.0] * cols_b for _ in range(rows_a)]
    for i, row_a in enumerate(mata):
        for j in range(cols_b):
            result[i][j] = sum(aik * matb[k][j] for k, aik in enumerate(row_a))
    return result


class LU:
    """LU decomposition

    Attributes:
        lower: Lower triangular matrix L.
        upper: Upper triangular matrix U.
        pivot: Pivot matrix P.
    """

    def __init__(self) -> None:
        pass

    def _row_exchanges(self) -> int:
        diag_dif_one_count = sum(self.pivot[i][i] != 1 for i in range(len(self.pivot)))
        return 0 if diag_dif_one_count == 0 else diag_dif_one_count - 1

    def _forward_substitution(self, b: Matrix) -> Matrix:
        """Perform forward substitution to solve Ly = b for y.

        Args:
            b: The right-hand side matrix/vector.

        Returns:
            Matrix: The solution vector y.
        """
        x = [[0.0] * len(r) for r in b]
        for i, bi in enumerate(b):
            for j, v in enumerate(bi):
                x[i][j] = v - sum(self.lower[i][k] * x[k][j] for k in range(i))
        return x

    def _backward_substitution(self, b: Matrix) -> Matrix:
        """Perform backward substitution to solve Ux = y for x.


        Args:
            b: The right-hand side matrix

        Returns:
            The solution vector x
        """
        n = len(b)
        x = [[0.0] * len(r) for r in b]
        for i in range(n - 1, -1, -1):
            for j in range(len(b[i])):
                x[i][j] = b[i][j] - sum(
                    self.upper[i][k] * x[k][j] for k in range(i + 1, n)
                )
                x[i][j] /= self.upper[i][i]
        return x

    def _solve(self, b: Matrix) -> Matrix:
        b = _matmul(self.pivot, b)
        y = self._forward_substitution(b)
        x = self._backward_substitution(y)
        return x

    def solve(self, b: Matrix | Vector | None = None) -> Vector | Matrix:
        """Solve linear system for Ax = b

        If b is not provided assumes that it is the identity matrix
        and computes the inverse of A.

        Args:
            b: right hand side matrix or vector. If not provided, assumes that
                `b` is the identity matrix and computes the inverse.

        Returns:
            The solution x with the same dimensions as b.
        """
        is_b_vec = False
        if b is None:
            b = [[0.0] * len(self.pivot) for _ in range(len(self.pivot))]
            for i in range(len(b)):
                b[i][i] = 1.0
        elif not isinstance(b[0], list):
            is_b_vec = True
            b = [[v] for v in b]  # type: ignore
        solution = self._solve(b)  # type: ignore
        if is_b_vec:
            return [v[0] for v in solution]
        return solution

    def det(self) -> float:
        det = self.upper[0][0]
        for i in range(1, len(self.upper)):
            det *= self.upper[i][i]
        return det if self._row_exchanges() % 2 == 0 else -det

    def fit(self, matrix: Matrix, tol: float = 1e-8) -> Self:
        """Fit LU factorization

        Args:
            matrix: Matrix to be factored into PA = LU.
            tol: Tolerance to identify singularity

        Raises:
            RuntimeError: If matrix is singular.
            ValueError: If matrix is not square.

        Returns:
            The class LU with attributes lower, upper and pivot.
        """
        if not _is_square_matrix(matrix):
            raise ValueError("Matrix is not square")
        size = len(matrix)
        self.lower = [[0.0] * size for _ in range(size)]
        self.upper = [[0.0] * size for _ in range(size)]
        self.pivot = _pivotize(matrix)
        matrix = _matmul(self.pivot, matrix)

        for i in range(size):
            self.lower[i][i] = 1.0

            for j in range(i, size):
                subtraction_term = sum(
                    self.upper[k][j] * self.lower[i][k] for k in range(i)
                )
                self.upper[i][j] = matrix[i][j] - subtraction_term

            if abs(self.upper[i][i]) < tol:
                raise RuntimeError("Matrix is singular")

            for j in range(i + 1, size):
                self.lower[j][i] = matrix[j][i]
                self.lower[j][i] -= sum(
                    self.upper[k][i] * self.lower[j][k] for k in range(i)
                )
                self.lower[j][i] /= self.upper[i][i]

        return self
